﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resource/master/plantilla.Master" AutoEventWireup="true" CodeBehind="crud_usuarios.aspx.cs" Inherits="practica2.paginas.crud_usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 156px;
        }

        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="GESTION DE USUARIOS"></asp:Label>
    <br />
    <br />
    <br />
    <asp:Label ID="Label2" runat="server" Text="CREAR USUARIOS"></asp:Label>
    <br />
    <br />
    <table style="width: 100%;">
        <tr>
            <td class="auto-style1">CODIGO:</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" onkeypress="javascript:return solonumeros(event)"></asp:TextBox>
                <script>
                    function solonumeros(e) {
                        var key;
                        if (window.event) // IE
                        {
                            key = e.keyCode;
                        }
                        else if (e.which) // Netscape/Firefox/Opera
                        {
                            key = e.which;
                        }

                        if (key < 48 || key > 57) {
                            return false;
                        }
                        return true;
                    }

                </script>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">USUARIO:</td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" Width="158px"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">CONTRASENA:</td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">NOMBRE:</td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">APELLIDO:</td>
            <td>
                <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            </td>

        </tr>
    </table>
    <br />
    <asp:Button ID="Button1" runat="server" Text="CREAR USUARIO" OnClick="Button1_Click" />
    <br />
    <br />
    MODIFICAR USUARIOS<br />
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server">
    </asp:DropDownList>
    <br />
    <table style="width: 100%;">
        <tr>
            <td class="auto-style1">USUARIO:</td>
            <td>
                <asp:TextBox ID="TextBox6" runat="server" Width="157px"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">CONTRASENA:</td>
            <td>
                <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">NOMBRE:</td>
            <td>
                <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="auto-style1">APELLIDO:</td>
            <td>
                <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
            </td>

        </tr>
    </table>
    <br />
    <asp:Button ID="Button3" runat="server" Text="EDITAR USUARIO" OnClick="Button3_Click" />
    <br />
    <br />
    ELIMINAR USUARIOS<br />
<br />
<br />
    CODIGO USUARIO:<br />
    <asp:DropDownList ID="DropDownList2" runat="server" Height="39px" Width="212px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Button ID="Button2" runat="server" Text="ELIMINAR USUARIO" OnClick="Button2_Click" />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1">
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:practica2ConnectionString %>" ProviderName="<%$ ConnectionStrings:practica2ConnectionString.ProviderName %>" SelectCommand="SELECT cod_usuario, usuario, password, nombre, apellido FROM usuario"></asp:SqlDataSource>
    <br />
    <br />
</asp:Content>
