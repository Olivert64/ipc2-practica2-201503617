﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using practica2.datos;
using MySql.Data.MySqlClient;

namespace practica2.paginas
{
    public partial class crud_usuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            llenarDrop();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //obtener los datos
            string codigo = Convert.ToString(TextBox1.Text);
            string usuario = Convert.ToString(TextBox2.Text);
            string contrasena = Convert.ToString(TextBox3.Text);
            string nombre = Convert.ToString(TextBox4.Text);
            string apellido = Convert.ToString(TextBox5.Text);

            usuarios ingresar = new usuarios();

            ingresar.agregarUsuario(codigo, usuario, contrasena, nombre, apellido);

            //TextBox1.Text = "";
            //TextBox2.Text = "";
            //TextBox3.Text = "";
            //TextBox4.Text = "";
            //TextBox5.Text = "";

            Response.Redirect("/paginas/crud_usuarios.aspx", true);
        }

        public void llenarDrop()
        {
            MySqlConnection conectar = conexion.RetornarConexion();

            try
            {
                //abrir conexion
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "SELECT cod_usuario FROM usuario WHERE cod_usuario != 1;";
                
                try
                {
                    MySqlDataReader reader;
                    reader = comando.ExecuteReader();

                    while (reader.Read())
                    {
                        String value = reader.GetString("cod_usuario");
                        DropDownList1.Items.Add(value);
                        DropDownList2.Items.Add(value);

                    }
                    //cerrar conexion
                    conectar.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //obtener datos
            String codigo = DropDownList1.SelectedValue;
            String usuario = Convert.ToString(TextBox6.Text);
            String contrasena = Convert.ToString(TextBox7.Text);
            String nombre = Convert.ToString(TextBox8.Text);
            String apellido = Convert.ToString(TextBox9.Text);

            usuarios editar = new usuarios();
            editar.editarUsuario(codigo, usuario, contrasena, nombre, apellido);

            Response.Redirect("/paginas/crud_usuarios.aspx",true);

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
           
            String codigo = DropDownList2.SelectedValue;     

            usuarios eliminar = new usuarios();
            eliminar.eliminarUsuario(codigo);

            Response.Redirect("/paginas/crud_usuarios.aspx", true);

        }
    }
}