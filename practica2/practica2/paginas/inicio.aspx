﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inicio.aspx.cs" Inherits="practica2.paginas.inicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../resource/css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />

</head>
<body>
    <div class="container well contenedor">

        <div>
            <div class="col-xs-12">
                <h2>INICIAR SESION</h2>
            </div>
        </div>

        <form id="form1" runat="server" class="form-horizontal">

            <div class="form-group">
                <asp:Label ID="lblusuario" runat="server" Text="Usuario" CssClass="custom-control-label col-sm-2"></asp:Label>
                <div class="col-sm-12">
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblpassword" runat="server" Text="Contraseña" CssClass="custom-control-label col-sm-2"></asp:Label>
                <div class="col-sm-12">
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Button ID="Button1" runat="server" Text="Iniciar Sesion" OnClick="Button1_Click" CssClass="form-control btn btn-dark" />
            </div>

        </form>

    </div>

</body>
</html>
