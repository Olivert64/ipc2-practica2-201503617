﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using practica2.datos;

namespace practica2.paginas
{
    public partial class inicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String usuario = Convert.ToString(TextBox1.Text);
            String contrasena = Convert.ToString(TextBox2.Text);

            validarLogin val = new validarLogin();

            try
            {
                String password = val.verificarC(usuario);
                if (password.Equals(contrasena))
                {
                    System.Diagnostics.Debug.Write("usuario y contrase;a correcto");
                    Response.Redirect("/paginas/crud_usuarios.aspx", true);
                    Response.Write("<script>window.alert('Bienvenido al sistema');</script>");
                }
                else
                {
                    System.Diagnostics.Debug.Write("usuario y contrase;a incorrecto");
                    Response.Write("<script>window.alert('nombre de usuario y contrase;a incorrectos');</script>");
                }
            }
            catch
            {
                System.Diagnostics.Debug.Write("usuario no existe");
                Response.Write("<script>window.alert('usuario no existe');</script>");
            }
        }
    }
}