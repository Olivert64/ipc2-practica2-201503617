﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace practica2.datos
{
    public class usuarios
    {
        //agregar usuarios
        public void agregarUsuario(String codigo, String usuario, String contrasena, String nombre, String apellido)
        {
            //objeto de conexion
            MySqlConnection conectar = conexion.RetornarConexion();

            try
            {
                //abrir conexion
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();


                comando.Connection = conectar;
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO usuario VALUES(@a, @b, @c, @d, @e);";


                //enlazar parametros
                comando.Parameters.AddWithValue("@a", codigo);
                comando.Parameters.AddWithValue("@b", usuario);
                comando.Parameters.AddWithValue("@c", contrasena);
                comando.Parameters.AddWithValue("@d", nombre);
                comando.Parameters.AddWithValue("@e", apellido);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        public void editarUsuario(String codigo, String usuario, String contrasena, String nombre, String apellido)
        {
            //objeto de conexion
            MySqlConnection conectar = conexion.RetornarConexion();

            try
            {
                //abrir conexion
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();


                comando.Connection = conectar;
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE usuario SET usuario = @b, password = @c, nombre = @d, apellido = @e WHERE cod_usuario = @a";


                //enlazar parametros
                comando.Parameters.AddWithValue("@a", codigo);
                comando.Parameters.AddWithValue("@b", usuario);
                comando.Parameters.AddWithValue("@c", contrasena);
                comando.Parameters.AddWithValue("@d", nombre);
                comando.Parameters.AddWithValue("@e", apellido);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        public void eliminarUsuario(String codigo)
        {
            //objeto de conexion
            MySqlConnection conectar = conexion.RetornarConexion();

            try
            {
                //abrir conexion
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();


                comando.Connection = conectar;
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE FROM usuario WHERE cod_usuario = @a";


                //enlazar parametros
                comando.Parameters.AddWithValue("@a", codigo);
                //comando.Parameters.AddWithValue("@b", usuario);
               // comando.Parameters.AddWithValue("@c", contrasena);
                //comando.Parameters.AddWithValue("@d", nombre);
               // comando.Parameters.AddWithValue("@e", apellido);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }
    }
}