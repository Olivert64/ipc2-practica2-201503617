﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace practica2.datos
{
    public class validarLogin
    {

        public String verificarC(String usuario)
        {
            MySqlConnection conectar = conexion.RetornarConexion();

            try
            {
                //abrir conexion
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();


                comando.Connection = conectar;
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "SELECT password FROM usuario WHERE usuario = @a;";


                //enlazar parametros
                comando.Parameters.AddWithValue("@a", usuario);               

                try
                {
                    String password = (comando.ExecuteScalar()).ToString();
                    //comando.ExecuteNonQuery();
                    conectar.Close();
                    return password;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    conectar.Close();
                    return null;
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }


            
        }


    }
}