CREATE DATABASE practica2;

USE practica2;

CREATE TABLE usuario(
    cod_usuario INTEGER NOT NULL PRIMARY KEY,
    usuario VARCHAR(30),
    password VARCHAR(20),
    nombre VARCHAR(30),
    apellido VARCHAR(30)
);

CREATE TABLE tipo(
    cod_tipo INTEGER NOT NULL PRIMARY KEY,
    nombre VARCHAR(30)
);

CREATE TABLE material(
    cod_material INTEGER NOT NULL PRIMARY KEY,
    nombre VARCHAR(30)
);

CREATE TABLE color(
    cod_color INTEGER NOT NULL PRIMARY KEY,
    nombre VARCHAR(30)
);

CREATE TABLE producto(
    cod_producto INTEGER NOT NULL PRIMARY KEY,
    descripcion VARCHAR(40),
    cantidad INTEGER,
    costo FLOAT,
    usuario_cod INTEGER NOT NULL,
    tipo_cod INTEGER NOT NULL,
    material_cod INTEGER NOT NULL,
    color_cod INTEGER NOT NULL
);


ALTER TABLE producto ADD FOREIGN KEY (usuario_cod) REFERENCES usuario(cod_usuario);
ALTER TABLE producto ADD FOREIGN KEY (tipo_cod) REFERENCES tipo(cod_tipo);
ALTER TABLE producto ADD FOREIGN KEY (material_cod) REFERENCES material(cod_material);
ALTER TABLE producto ADD FOREIGN KEY (color_cod) REFERENCES color(cod_color);

INSERT INTO usuario (cod_usuario, usuario, password, nombre, apellido)
VALUES (1,"admin","admin","Administrador","Administrador");

